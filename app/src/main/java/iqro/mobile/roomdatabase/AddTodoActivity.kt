package iqro.mobile.roomdatabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import iqro.mobile.roomdatabase.data.Todo
import iqro.mobile.roomdatabase.data.TodoDatabase
import iqro.mobile.roomdatabase.databinding.ActivityAddTodoBinding
import kotlinx.coroutines.launch

class AddTodoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddTodoBinding
    private lateinit var database: TodoDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddTodoBinding.inflate(layoutInflater)
        setContentView(binding.root)


        database = TodoDatabase.getInstance(this)

        binding.floatingActionButton.setOnClickListener {
            lifecycleScope.launch {
                val title = binding.titleEt.text.toString()
                val description = binding.descriptionEt.text.toString()
                val date = binding.chooseDateBtn.text.toString()
                database.todoDao().insert(Todo(null, title, description, date, "", "", false))
                finish()
            }
        }

    }
}