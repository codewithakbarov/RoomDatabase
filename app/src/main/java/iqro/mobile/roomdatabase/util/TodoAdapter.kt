package iqro.mobile.roomdatabase.util

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import iqro.mobile.roomdatabase.data.Todo
import iqro.mobile.roomdatabase.databinding.ItemTodoBinding

/**
 *Created by Zohidjon Akbarov
 */

class TodoDiff() : DiffUtil.ItemCallback<Todo>() {
    override fun areItemsTheSame(oldItem: Todo, newItem: Todo): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Todo, newItem: Todo): Boolean {
        return oldItem == newItem
    }

}

class TodoAdapter : ListAdapter<Todo, TodoAdapter.TodoViewHolder>(TodoDiff()) {

    private var listener: TodoListener? = null

    inner class TodoViewHolder(private val binding: ItemTodoBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.checkBox.setOnCheckedChangeListener { checkBox, isChecked ->
                if (checkBox.isPressed)
                    listener?.checkBoxClicked(getItem(adapterPosition), isChecked)
            }
        }

        fun bind(todo: Todo) {
            binding.titleTv.text = todo.title
            binding.descriptionTv.text = todo.description
            binding.dateTv.text = todo.createdAt
            binding.checkBox.isChecked = todo.isChecked

        }

    }

    fun setListener(listener: TodoListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val binding = ItemTodoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TodoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        holder.bind(getItem(position))

    }
}