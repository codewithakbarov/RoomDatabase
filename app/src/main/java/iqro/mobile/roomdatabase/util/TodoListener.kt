package iqro.mobile.roomdatabase.util

import iqro.mobile.roomdatabase.data.Todo

/**
 *Created by Zohidjon Akbarov
 */
interface TodoListener {
    fun checkBoxClicked(todo: Todo ,isChecked:Boolean)
}