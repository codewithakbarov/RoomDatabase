package iqro.mobile.roomdatabase.data

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import kotlinx.coroutines.flow.Flow

/**
 *Created by Zohidjon Akbarov
 */

@Dao
interface TodoDao {

    @Insert(onConflict = REPLACE)
    suspend fun insert(todo: Todo)

    @Insert
    suspend fun insertAll(todoList: List<Todo>)

    @Delete
    suspend fun delete(todo: Todo)

    @Update
    suspend fun update(todo: Todo)

    @Query("select * from todo limit 10")
    fun getAllTodo(): Flow<List<Todo>>

    @Query("select * from todo where id = :id")
    suspend fun getTodoById(id: Int): Todo?

}