package iqro.mobile.roomdatabase.async

import android.os.AsyncTask
import iqro.mobile.roomdatabase.data.Todo
import iqro.mobile.roomdatabase.data.TodoDao

/**
 *Created by Zohidjon Akbarov
 */
class GetTodoList(val todoDao: TodoDao, val getAllTodoCallBack: GetAllTodoCallBack) :
    AsyncTask<Unit, Unit, List<Todo>>() {
    override fun doInBackground(vararg p0: Unit?): List<Todo> {
//        return todoDao.getAllTodo()
        return emptyList()
    }

    override fun onPostExecute(result: List<Todo>?) {
        super.onPostExecute(result)
        getAllTodoCallBack.onAllListGet(result)
    }
}