package iqro.mobile.roomdatabase.async

/**
 *Created by Zohidjon Akbarov
 */
interface AddTodoCallBack {
    fun onControlProgressBar()
    fun onFinish()
}