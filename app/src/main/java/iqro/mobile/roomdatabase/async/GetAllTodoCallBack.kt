package iqro.mobile.roomdatabase.async

import iqro.mobile.roomdatabase.data.Todo

/**
 *Created by Zohidjon Akbarov
 */
interface GetAllTodoCallBack {
    fun onAllListGet(list:List<Todo>?)
}