package iqro.mobile.roomdatabase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import iqro.mobile.roomdatabase.data.Todo
import iqro.mobile.roomdatabase.data.TodoDatabase
import iqro.mobile.roomdatabase.databinding.ActivityMainBinding
import iqro.mobile.roomdatabase.util.TodoAdapter
import iqro.mobile.roomdatabase.util.TodoListener
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var database: TodoDatabase
    private lateinit var binding: ActivityMainBinding
    val TAG = "TAG"
    var count = 0
    private lateinit var todoAdapter: TodoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        database = TodoDatabase.getInstance(this)
        todoAdapter = TodoAdapter()

        lifecycleScope.launch {
            database.todoDao().getAllTodo().collect { listToDo ->
                count = listToDo.size
                todoAdapter.submitList(listToDo)
            }
        }

        binding.fab.setOnClickListener {
            Intent(this, AddTodoActivity::class.java).apply {
                startActivity(this)
            }
        }

        todoAdapter.setListener(object : TodoListener {
            override fun checkBoxClicked(todo: Todo, isChecked: Boolean) {
                lifecycleScope.launch {
                    todo.isChecked = isChecked
                    database.todoDao().update(todo)
                }
            }
        })

        binding.recyclerView.apply {
            adapter = todoAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

    }
}